(ns addon
  (:import (org.springframework.boot.autoconfigure SpringBootApplication)
           (org.springframework.boot SpringApplication)))

(defn -main [& args] (SpringApplication/run (Class/forName "addon.AddonApplication") (into-array String args)))

(gen-class :name ^{SpringBootApplication true} addon.AddonApplication :main true)
