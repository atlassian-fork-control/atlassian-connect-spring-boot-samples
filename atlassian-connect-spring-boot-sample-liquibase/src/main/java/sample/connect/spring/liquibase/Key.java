package sample.connect.spring.liquibase;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Key {

    @Id
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
