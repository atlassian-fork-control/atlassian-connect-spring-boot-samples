package sample.connect.spring.liquibase;

import org.springframework.data.repository.CrudRepository;

public interface KeyRepository extends CrudRepository<Key, String> {}
